import * as React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import ContactsScene from './scenes/ContactsScene';
import ContactScene from './scenes/ContactScene';

const Stack = createStackNavigator();

function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Contacts" component={ContactsScene} />
        <Stack.Screen name="Contact" component={ContactScene} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;
