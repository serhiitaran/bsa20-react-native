import * as React from 'react';
import {StyleSheet, View, Text, Image, TouchableHighlight} from 'react-native';
import Contacts from 'react-native-contacts';
import call from 'react-native-phone-call';
import Icon from 'react-native-vector-icons/FontAwesome';

const ContactScene = ({navigation, route}) => {
  const {avatar, name, phone, recordID} = route.params;

  const onDelete = () => {
    Contacts.deleteContact({recordID}, (err, recordId) => {
      if (err) {
        throw err;
      }
      navigation.navigate('Contacts', {deletedContact: recordId});
    });
  };

  const onCall = () => {
    const args = {
      number: phone,
      prompt: false,
    };

    call(args).catch(console.error);
  };

  return (
    <View style={styles.contactContainer}>
      <Image source={avatar} style={styles.image} />
      <View style={styles.contactInfo}>
        <Text style={styles.contactInfoTitle}>Name</Text>
        <Text style={styles.contactInfoValue}>{name}</Text>
      </View>
      <View style={styles.contactInfo}>
        <Text style={styles.contactInfoTitle}>Phone number</Text>
        <Text style={styles.contactInfoValue}>{phone}</Text>
      </View>
      <View style={styles.contactButtons}>
        <TouchableHighlight onPress={onCall}>
          <View style={styles.contactButton}>
            <Icon style={styles.contactButtonCallIcon} name="phone" />
            <Text style={styles.contactButtonTitle}> Call </Text>
          </View>
        </TouchableHighlight>

        <TouchableHighlight onPress={onDelete}>
          <View style={styles.contactButton}>
            <Text style={styles.contactButtonTitle}> Delete </Text>
            <Icon style={styles.contactButtonDeleteIcon} name="times" />
          </View>
        </TouchableHighlight>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  contactContainer: {
    flex: 1,
    alignItems: 'center',
    padding: 25,
    backgroundColor: 'white',
  },
  image: {
    width: 125,
    height: 125,
    borderWidth: 1,
    borderColor: 'black',
  },
  contactInfo: {
    alignSelf: 'stretch',
    borderBottomWidth: 1,
    borderBottomColor: '#f3f3f3',
    marginTop: 50,
    paddingBottom: 10,
  },

  contactInfoTitle: {
    fontSize: 14,
    marginBottom: 15,
  },

  contactInfoValue: {
    fontSize: 17,
    fontWeight: 'bold',
  },

  contactButtons: {
    alignSelf: 'stretch',
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 75,
  },

  contactButton: {
    width: 120,
    backgroundColor: '#dfdfdf',
    flexDirection: 'row',
    paddingHorizontal: 20,
    paddingVertical: 10,
    alignItems: 'center',
    borderRadius: 20,
  },

  contactButtonTitle: {
    textTransform: 'uppercase',
    fontWeight: 'bold',
  },

  contactButtonCallIcon: {
    marginRight: 10,
    color: 'gray',
    fontSize: 20,
  },

  contactButtonDeleteIcon: {
    marginLeft: 10,
    color: 'gray',
    fontSize: 20,
  },
});

export default ContactScene;
