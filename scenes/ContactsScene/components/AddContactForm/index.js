import React, {useState} from 'react';
import {
  Text,
  View,
  TextInput,
  TouchableOpacity,
  StyleSheet,
} from 'react-native';

const AddContactFrom = ({onClose, onContactAdd}) => {
  const [firstName, setFirstName] = useState('');
  const [secondName, setSecondName] = useState('');
  const [phone, setPhone] = useState('');

  const onSubmit = () => {
    if (!firstName.length || !phone.length) {
      return;
    }
    onContactAdd({firstName, secondName, phone});
    onClose();
  };
  return (
    <View style={styles.formContainer}>
      <Text style={styles.formHeader}> Add contact </Text>
      <View>
        <Text>First name (required)</Text>
        <TextInput
          style={styles.formInput}
          placeholder="Enter First Name"
          underlineColorAndroid="transparent"
          value={firstName}
          onChangeText={setFirstName}
        />
        <Text>Second name</Text>
        <TextInput
          style={styles.formInput}
          placeholder="Enter Second Name"
          underlineColorAndroid="transparent"
          value={secondName}
          onChangeText={setSecondName}
        />
        <Text>Phone (required)</Text>
        <TextInput
          style={styles.formInput}
          placeholder="Enter phone"
          underlineColorAndroid="transparent"
          value={phone}
          onChangeText={setPhone}
        />
        <View style={styles.formButtons}>
          <TouchableOpacity style={styles.formButton} onPress={onSubmit}>
            <Text style={styles.formButtonTitle}>Add contact</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.formButton} onPress={onClose}>
            <Text style={styles.formButtonTitle}>Cancel</Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  formContainer: {
    alignSelf: 'stretch',
    alignItems: 'stretch',
  },
  formHeader: {
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center',
    textTransform: 'uppercase',
    marginBottom: 35,
  },
  formInput: {
    borderWidth: 0,
    backgroundColor: '#e7e9f4',
    marginTop: 10,
    marginBottom: 20,
    padding: 10,
    borderRadius: 5,
  },
  formButtons: {
    flexDirection: 'row',
    marginTop: 20,
    marginBottom: 15,
  },
  formButton: {
    marginRight: 20,
    color: 'black',
    width: 120,
    backgroundColor: '#dfdfdf',
    padding: 10,
    borderRadius: 15,
  },
  formButtonTitle: {
    fontWeight: 'bold',
    textAlign: 'center',
  },
});

export default AddContactFrom;
