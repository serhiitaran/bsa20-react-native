import React from 'react';
import {StyleSheet, FlatList, View, Text, TouchableOpacity} from 'react-native';
import {SwipeRow} from 'react-native-swipe-list-view';
import Icon from 'react-native-vector-icons/FontAwesome';

const ContactList = ({
  contacts,
  onContactDelete,
  onContactDetails,
  move,
  moveEnd,
}) => {
  const contactItem = ({item}) => {
    if (!item.phoneNumbers.length) {
      return null;
    }
    const avatar = item.hasThumbnail
      ? {uri: item.thumbnailPath}
      : require('./default-avatar.png');
    const name = `${item.givenName} ${item.familyName}`;
    const phone = item.phoneNumbers[0].number;
    const recordID = item.recordID;
    return (
      <SwipeRow rightOpenValue={-75}>
        <TouchableOpacity onPress={() => onContactDelete(recordID)}>
          <Icon style={styles.contactItemDelete} name="trash" size={30} />
        </TouchableOpacity>
        <TouchableOpacity
          activeOpacity={1}
          onLongPress={move}
          onPressOut={moveEnd}
          onPress={() =>
            onContactDetails({
              avatar,
              name,
              phone,
              recordID,
            })
          }>
          <View style={styles.contactItem}>
            <Icon name="address-book" size={25} color="grey" />
            <View style={styles.contactItemContent}>
              <View>
                <Text style={styles.contactItemName}>{name}</Text>
                <Text style={styles.contactItemPhone}>{phone}</Text>
              </View>
              <Icon name="info" size={18} color="grey" />
            </View>
          </View>
        </TouchableOpacity>
      </SwipeRow>
    );
  };

  return (
    <FlatList
      data={contacts}
      renderItem={contactItem}
      keyExtractor={(item, index) => index.toString()}
    />
  );
};

const styles = StyleSheet.create({
  contactItem: {
    backgroundColor: '#e7e9f4',
    paddingHorizontal: 50,
    paddingVertical: 10,
    margin: 10,
    flexDirection: 'row',
    alignItems: 'center',
  },
  contactItemContent: {
    marginLeft: 50,
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  contactItemName: {
    color: 'black',
    fontWeight: 'bold',
    fontSize: 18,
  },
  contactItemPhone: {
    color: 'gray',
    fontSize: 18,
    marginTop: 5,
  },
  contactItemDelete: {
    alignSelf: 'flex-end',
    marginRight: 20,
    marginTop: 15,
    padding: 15,
    backgroundColor: '#dfdfdf',
  },
});

export default ContactList;
