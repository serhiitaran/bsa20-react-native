/* eslint-disable no-useless-escape */
import React, {useState, useEffect} from 'react';
import {PermissionsAndroid, Platform, View, StyleSheet} from 'react-native';
import Contacts from 'react-native-contacts';
import ContactList from './components/ContactList';
import SearchBar from './components/SearchBar';
import AddContact from './components/AddContact';

const ContactsScene = ({navigation, route}) => {
  const [currentContacts, setCurrentContacts] = useState(undefined);
  useEffect(() => {
    async function getContacts() {
      if (Platform.OS === 'android') {
        PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.READ_CONTACTS,
          {
            title: 'Contacts',
            message: 'This app would like to view your contacts.',
          },
        ).then(() => {
          loadContacts();
        });
      } else {
        loadContacts();
      }
    }
    getContacts();
  }, []);

  const loadContacts = () => {
    Contacts.getAll((err, contacts) => {
      if (err === 'denied') {
        console.warn('Permission to access contacts was denied');
      } else {
        setCurrentContacts(contacts);
      }
    });
  };

  useEffect(() => {
    if (route.params?.deletedContact) {
      loadContacts();
    }
  }, [route.params]);

  const search = (text) => {
    const phoneNumberRegex = /\b[\+]?[(]?[0-9]{2,6}[)]?[-\s\.]?[-\s\/\.0-9]{3,15}\b/m;
    const emailAddressRegex = /^(([^<>()[\].,;:\s@"]+(\.[^<>()[\].,;:\s@"]+)*)|(".+"))@(([^<>()[\].,;:\s@"]+\.)+[^<>()[\].,;:\s@"]{2,})$/i;
    if (text === '' || text === null) {
      loadContacts();
    } else if (phoneNumberRegex.test(text)) {
      Contacts.getContactsByPhoneNumber(text, (_err, contacts) => {
        setCurrentContacts(contacts);
      });
    } else if (emailAddressRegex.test(text)) {
      Contacts.getContactsByEmailAddress(text, (_err, contacts) => {
        setCurrentContacts(contacts);
      });
    } else {
      Contacts.getContactsMatchingString(text, (_err, contacts) => {
        setCurrentContacts(contacts);
      });
    }
  };

  const addContact = (newPerson) => {
    Contacts.addContact(newPerson, (err) => {
      if (err) {
        console.warn(err);
      } else {
        loadContacts();
      }
    });
  };

  const onContactAdd = async ({firstName, secondName, phone}) => {
    const newPerson = {
      phoneNumbers: [
        {
          label: 'mobile',
          number: phone,
        },
      ],
      familyName: secondName,
      givenName: firstName,
    };
    if (Platform.OS === 'android') {
      PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.WRITE_CONTACTS,
        {
          title: 'Contacts',
          message: 'This app would like to add new contact.',
        },
      ).then(() => {
        addContact(newPerson);
        loadContacts();
      });
    }
  };

  const onContactDelete = (recordID) => {
    Contacts.deleteContact({recordID}, (err, recordId) => {
      if (err) {
        throw err;
      }
      loadContacts();
    });
  };

  const onContactDetails = (contact) => {
    navigation.navigate('Contact', contact);
  };

  return (
    <View style={styles.contactsScene}>
      {currentContacts ? (
        <>
          <AddContact onContactAdd={onContactAdd} />
          <SearchBar onChangeText={search} searchPlaceholder={'Search'} />
          {currentContacts.length ? (
            <ContactList
              contacts={currentContacts}
              onContactDetails={onContactDetails}
              onContactDelete={onContactDelete}
            />
          ) : null}
        </>
      ) : null}
    </View>
  );
};
const styles = StyleSheet.create({
  contactsScene: {
    flex: 1,
    justifyContent: 'flex-start',
    paddingVertical: 50,
    backgroundColor: '#fff',
  },
});

export default ContactsScene;
